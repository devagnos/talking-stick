TalkingStick::Engine.routes.draw do

  get "/logout" => "devise/sessions#destroy"
  get '/delete' => 'documents#destroy'

  devise_for :users, :controllers => { :omniauth_callbacks => "users/omniauth_callbacks" }
  resources :users do
    resources :documents
  end
  get "rooms/:room_id/checkout" => "rooms#checkout", :as => :checkout
  post "rooms/:room_id/confirmation" => "rooms#confirmation", :as => :confirmation

  resources :documents
  resources :rooms do
    resources :documents
    resources :messages
    resources :participants do
      post 'signals', to: 'rooms#signal'
    end
  end
end