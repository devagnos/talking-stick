require_dependency "talking_stick/application_controller"

module TalkingStick
  class DocumentsController < ApplicationController
    #before_action :set_room, only: [:index]
    #before_action :set_user, only: [:index]
    before_action :authenticate_user!
    before_action :set_document, only: [:show, :edit, :update, :destroy]

    layout 'temp_tpl'

    # GET /documents
    def index
      @documents = []
      @owner_documents = []
      @rooms = Room.where(owner_id: current_user.id).or(Room.where(user_id: current_user.id))

      @rooms.each do |room|
        @documents = @documents + room.documents.where.not(user_id: current_user.id)
        @owner_documents = @owner_documents + room.documents.where(user_id: current_user.id)
      end

    end

    # GET /documents/1
    def show
      @documents = @room.documents
    end

    # GET /documents/new
    def new
      #@room = Room.find(params[:id])
      @document = Document.new
      respond_to do |format|
        format.html { render :layout => !request.xhr? }
      end

    end

    # GET /documents/1/edit
    def edit
      @room = Room.find(params[:id]).documents
    end

    # POST /documents
    def create
      #@document = Document.new(:room_id => params[:room_id], :name => params[:name], :attachment => params[:attachment], :user_id => current_user.id)

      @document = Document.new(document_params)


      if @document.save

        respond_to :js
      end
      #format.html { redirect_to @room, notice: 'Document was successfully created.' }


    end

    # PATCH/PUT /documents/1
    def update
      if @document.update(document_params)
        redirect_to documents_path, notice: 'Document was successfully updated.'
      else
        render :edit
      end
    end

    # DELETE /documents/1
    def destroy
      @document = Document.find(params[:id])

        if @document.user_id == current_user.id
        @document.delete


        respond_to do |format|
          format.html { redirect_to documents_path }
          format.js

        end

      end
    end

    private

    def set_room
      @room = Room.find(params[:room_id])
    end

    def set_user
      @user = User.find(params[:id])
    end


    # Use callbacks to share common setup or constraints between actions.
    def set_document
      @document = Document.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def document_params
      params.require(:document).permit(:name, :user_id, :attachment, :room_id, :hidden)
    end
  end
end
