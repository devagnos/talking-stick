require_dependency "talking_stick/application_controller"

module TalkingStick
  class RoomsController < ApplicationController
    #before_action :authenticate_user!
    before_action :set_room, only: [:show, :edit, :update, :destroy, :signal]
    before_action :set_participant, only: [:signal]

    layout 'temp_tpl'

    # GET /rooms
    def index
      @rooms = Room.all
      @users = User.all
      @reservations = Reservation.all


        # if params[:token]
      #   token = params[:token]
      #   invoice = Paydunya::Checkout::Invoice.new
      #   if invoice.confirm(token)
      #     if invoice.status == "completed"
      #       @receipt = invoice.receipt_url
      #       @this_room = invoice.get_custom_data "room"
      #       @room_payed = @rooms.find(@this_room)
      #       @room_payed.update(payed_for: true, receipt: @receipt)
      #       if @room_payed.owner.account_alias.present?
      #         direct_pay = Paydunya::DirectPay.new
      #         account_alias = @room_payed.owner.account_alias
      #         if @room_payed.cour.currency.currency_type == "€"
      #         prix_mentor = @room_payed.direct_pay_mentor
      #           if direct_pay.credit_account(account_alias, prix_mentor)
      #             @room_payed.update(payed: true)
      #           end
      #         elsif @room_payed.cour.currency.currency_type == "CFA"
      #           prix_mentor = @room_payed.p_mentor
      #           if direct_pay.credit_account(account_alias, prix_mentor)
      #             @room_payed.update(payed: true)
      #           end
      #         end
      #
      #       end
      #       redirect_to rooms_path
      #     else
      #       redirect_to rooms_path
      #     end
      #   else
      #     puts invoice.status
      #     puts invoice.response_text
      #     puts invoice.response_code
      #
      #   end
      #
      # end
    end

    def checkout

      # We will programatically simulate the last 6 cart items as checkout items
      #@items = Cart.order("created_at DESC").limit(6)
      @room = TalkingStick::Room.find(params[:room_id])


      co = Paydunya::Checkout::Invoice.new

      # co.cancel_url = "http://localhost:3000"
      # co.return_url = "http://localhost:3000"
      if @room.cour.currency.currency_type == '€'
        price = @room.unit_price_euro
        total_amount = 0.0
        #@items.each do | item |
        name = @room.name
        quantity = 1
        total_price = @room.euro_to_cfa
        co.add_item(name, quantity, price, total_price)
        co.add_custom_data("room", @room.id)
        total_amount += @room.euro_to_cfa
        #prix_uneur = @room.p_uneur

        #end
        co.total_amount += total_amount

      elsif @room.cour.currency.currency_type == 'CFA'
        price = @room.cour.price.abs.round(2)
        total_amount = 0.0
        #@items.each do | item |
        name = @room.name
        quantity = 1
        total_price = @room.p_etudiant
        co.add_item(name, quantity, price, total_price)
        co.add_custom_data("room", @room.id)
        total_amount += @room.p_etudiant
        co.total_amount += total_amount
      end

      if co.create
        redirect_to co.invoice_url
      else
        redirect_to rooms_path, :notice => co.response_text
      end

    end

    def stripe_payment
      @room = TalkingStick::Room.find(params[:room_id])

      if @room.cour.currency.currency_type == '€'
        @currency = 'eur'
      elsif @room.cour.currency.currency_type == 'CFA'
        @currency = 'xof'
      end
      @amount = @room.cour.price.abs.to_i * 100
      @distination_account = @room.owner.stripe_uid
      customer = Stripe::Customer.create(
          :email => params[:stripeEmail],
          :source => params[:stripeToken]
      )
      charge = Stripe::Charge.create({
                                         :customer => customer.id,
                                         :amount => @amount,
                                         :description => 'Paiement du cours',
                                         :currency => @currency
                                     })
      if charge.paid
        TalkingStick::Room.find(params[:room_id]).update(:payed_for => true)
        redirect_to '/rooms'
      else
        render :new
      end
    end

    # GET /rooms/1
    def show
      if (current_user.id == @room.user_id) || (current_user.id == @room.owner_id)
        @room.last_used = Time.now
        #@documents = @room.document
        #@room = Room.includes(:messages).find_or_create(slug: (params[:id] || params[:room_id]))
        @message = Message.new
        @document = Document.new
        @documents = Document.all
        @documents = @room.documents
        @reservations = Reservation.all


        @room.save

        if params[:guid]
          if @participant = Participant.where(guid: params[:guid]).first
            @participant.last_seen = Time.now
            @participant.save
          end
        end

        Participant.remove_stale! @room

        response = {
            room: @room,
            participants: @room.participants,
            signals: deliver_signals!,
        }

        respond_to do |format|
          format.html
          format.json { render json: response }
        end
      else
        redirect_to rooms_path
      end
    end

    # GET /rooms/new
    def new
      @room = Room.new
      @document = Document.new
    end

    # GET /rooms/1/edit
    def edit
    end

    # POST /rooms
    def create
      if (user_signed_in?)
        @room = Room.new(room_params)
      end
      if @room.save
        redirect_to @room, notice: 'Room was successfully created.'
      else
        render :new
      end
    end

    # PATCH/PUT /rooms/1
    def update
      if @room.update(room_params)
        redirect_to @room, notice: 'Room was successfully updated.'
      else
        render :edit
      end
    end

    # DELETE /rooms/1
    def destroy
      @room.destroy
      redirect_to rooms_url, notice: 'Room was successfully destroyed.'
    end

    def signal
      signal = signal_params
      signal[:room] = @room
      signal[:sender] = Participant.where(guid: signal[:sender]).first
      signal[:recipient] = @participant
      TalkingStick::Signal.create! signal
      head 204
    end

    def deliver_signals!
      data = TalkingStick::Signal.where recipient: @participant

      # Destroy the signals as we return them, since they have been delivered
      result = []
      data.each do |signal|
        result << {
            signal_type: signal.signal_type,
            sender_guid: signal.sender_guid,
            recipient_guid: signal.recipient_guid,
            data: signal.data,
            room_id: signal.room_id,
            timestamp: signal.created_at,
        }
      end
      data.delete_all
      result
    end

    private
    # Use callbacks to share common setup or constraints between actions.
    def set_room
      @room = Room.find(params[:id] || params[:room_id])
    end

    def set_participant
      @participant = Participant.find(params[:participant_id])
    rescue ActiveRecord::RecordNotFound
      # Retry with ID as GUID
      @participant = Participant.where(guid: params[:participant_id]).first
      raise unless @participant
    end

    # Only allow a trusted parameter "white list" through.
    def room_params
      params.require(:room).permit(:name, :last_used, :user_id, :cour_id, :reservation_id, :availability_id,
                                   :start_day, :start_hour, :duration, :started, :done, :payed_for, :reservation_id,
                                   :owner_id, :receipt, :payed, :price, :currency)
    end

    def signal_params
      params.permit(:sender, :signal_type, :data)
    end
  end
end
