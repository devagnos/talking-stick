module TalkingStick
  class Document < ApplicationRecord
    mount_uploader :attachment, AttachmentUploader
    belongs_to :user
    belongs_to :room
    validate :file

    def file
      if attachment.size.to_f > 2.megabytes.to_f
        error.add(:attachment, "file too heavy")
        return false
      else
        return true
      end
    end
  end
end
