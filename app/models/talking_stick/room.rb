module TalkingStick
  class Room < ActiveRecord::Base
    has_many :participants, dependent: :destroy
    has_many :signals, dependent: :destroy
    has_many :messages, dependent: :destroy
    has_many :documents
    belongs_to :reservation
    has_many :purchases
    belongs_to :cour
    has_many :buyers, through: :purchases
    belongs_to :owner, class_name: 'User'
    belongs_to :user


    def prix
      total_price.abs
    end

    def duration
     (self.end_time - self.start_time)
    end

    def seconds_to_time(seconds)
      [seconds / 3600, seconds / 60 % 60, seconds % 60].map { |t| t.to_s.rjust(2,'0') }.join(':')
    end

    def total_price
      (((self.end_time - self.start_time) / 3600.0) * self.cour.price.abs).round
    end

    def p_etudiant
      prix
    end

    def euro_rest
      (prix * 0.24) + 0.0762245
    end


    def p_mentor
      (prix - euro_rest).round
    end

    def p_uneur
      (prix - p_mentor).round
    end

    def p_uneur_rest
      (p_uneur - ((p_uneur * 0.24) + 0.0762245)).round
    end

    def p_uneur_cfa
      (p_uneur * 655.957).round
    end

    def p_uneur_rest_cfa
      (p_uneur - ((p_uneur * 0.24) + 50)).round
    end

    def euro_to_cfa
      (prix * 655.957).round
    end

    def unit_price_euro
      self.cour.price.abs.round * 655.957
    end

    def rest_price
      (euro_to_cfa * 0.24) + 50
    end

    def direct_pay_mentor
      (euro_to_cfa - rest_price).round
    end

    def tax_cfa
      ((p_etudiant * 0.04) + 50).round
    end

    def tax_eur
      ((euro_to_cfa * 0.04) + 50).round
    end
    #before_validation :sluggify_name

=begin
    def self.find_or_create(slug:)
      slug = slug.parameterize
      find_by(slug: slug) || create(name: slug.titleize, slug: slug)
    end

    def to_param
      slug
    end

    private

    def sluggify_name
      self.slug = name.parameterize unless slug.present?
    end
=end
  end
end
