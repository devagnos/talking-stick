# TalkingStick

This gem provides easy [WebRTC](https://webrtc.org) communication in any Rails app.


## Goals

* Provide easy group-based (2 or more participants) audio/video communication
* Allow selecting audio or audio+video
* Pluggable signaling delivery technology
* Plug & Play functionality - works out of the box
* Focus on simplicity at the API level, while maximizing the possibilities of integrating communication into existing Rails apps

## Installation

Note that TalkingStick requires jQuery, and installs [jquery-rails gem](https://github.com/rails/jquery-rails) as a dependency.

TalkingStick is built as a Rails Engine, and so follows those conventions.

1. Add to Gemfile and update the bundle

    ```Ruby
    gem 'talking_stick'
    ```

    Now run `bundle install` to download and install the gem.
2. Mount the engine
    Add the following to your application's `config/routes.rb`:

    ```Ruby
    mount TalkingStick::Engine, at: '/talking_stick'
    ```
3. Install and Run migration
    To add the required models to your application, the migrations must be copied and run:
    ```
    $ rake railties:install:migrations db:migrate
    ```

4. Import Assets
    Add the following to your application's `assets/javascripts/application.js`:

    ```javascript
    //= require talking_stick/application
    ```

    And the following to your application's `assets/stylesheets/application.scss`:

    ```css
    *= require talking_stick/application
    ```

5. **[optional]** Generate the default `participant` and `room` views which are then available for customization.  From your app's root directory:

   ```
   $ rails generate talking_stick:views
   ```

6. Videconference!
    After booting Rails, point your browser to something like [http://localhost:3000/talking_stick/rooms](http://localhost:3000/talking_stick/rooms). From there you will be able to create and join rooms.

For a better experience, we recommend also using [Bootstrap](http://getbootstrap.com). Easy installation of Bootstrap into Rails is available via [twitter-bootstrap-rails](https://github.com/seyhunak/twitter-bootstrap-rails).

## How it works

* Each room is assigned a unique ID
* Rails app keeps track of participants in each room
* Rails app notifies all room participants when one joins or leaves
* Rails app proxies call setup between participants in each room

## Extending TalkingStick

### Providing your own signaling mechanism

One of the critical decisions made by the standards bodies behind WebRTC was a decision to leave the signaling of WebRTC up to the application. This means that an application may use any means it desires to give the endpoints the data necessary to communicate directly.

This plugin tries to be "batteries included", which means that we included a signaling mechanism that will work on any Rails server. As such, we could not rely on any particular push technology, such as Websockets or Server Sent Events. Thus, the default behavior is that the browser polls the Rails API for updates on connected peers.

### Additional Methods

* `TalkingStick.parters` The collection of `TalkingStick.Partners` to whom the local session is currently connected. Treat it as read-only.
* `TalkingStick.log(level, obj[, obj ...])` Allows messages to be conditionally logged to the console. Valid levels are strings, and may be one of `trace`, `debug`, `notice`, `warning`, or `error`.

### Events

* `talking_stick.local_media_setup`: (no arguments) Triggered after local media has been initialized and drawn to the configured element
