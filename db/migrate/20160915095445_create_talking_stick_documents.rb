class CreateTalkingStickDocuments < ActiveRecord::Migration[5.0]
  def change
    create_table :talking_stick_documents do |t|
      t.string :name
      t.integer :user_id
      t.string :attachment
      t.integer :room_id

      t.timestamps
    end
  end
end
