class CreateTalkingStickRooms < ActiveRecord::Migration
  def change
    create_table :talking_stick_rooms do |t|
      t.string :name
      t.integer :user_id
      t.integer :cour_id
      t.integer :reservation_id
      t.integer :availability_id
      t.integer :start_day
      t.integer :start_hour
      t.integer :duration
      t.datetime :start_time
      t.datetime :end_time
      t.boolean :started
      t.boolean :done
      t.boolean :payed_for
      t.timestamp :last_used

      t.timestamps null: false
    end
  end
end
