class CreateTalkingStickMessages < ActiveRecord::Migration
  def change
    create_table :talking_stick_messages do |t|
      t.text :body
      t.references :user, foreign_key: true
      t.references :room, foreign_key: true
      t.timestamps
    end
  end
end
