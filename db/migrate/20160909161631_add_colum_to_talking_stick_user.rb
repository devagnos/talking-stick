class AddColumToTalkingStickUser < ActiveRecord::Migration[5.0]
  def change
    add_column :talking_stick_users, :first_name, :string
    add_column :users_stick_users, :last_name, :string
    add_column :users_stick_users, :about, :text
    add_column :users_stick_users, :avatar, :string
    add_column :users_stick_users, :cover, :string
    add_column :users_stick_users, :town_id, :integer
    add_column :users_stick_users, :country_id, :integer
    add_column :users_stick_users, :is_admin, :boolean
  end
end
