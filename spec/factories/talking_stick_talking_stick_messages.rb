FactoryGirl.define do
  factory :talking_stick_talking_stick_message, class: 'TalkingStick::TalkingStickMessage' do
    body "MyText"
    user nil
    room nil
  end
end
