FactoryGirl.define do
  factory :talking_stick_document, class: 'TalkingStick::Document' do
    name "MyString"
    user_id 1
    attachment "MyString"
    room_id 1
  end
end
