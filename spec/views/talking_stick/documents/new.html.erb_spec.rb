require 'rails_helper'

RSpec.describe "documents/new", type: :view do
  before(:each) do
    assign(:document, Document.new(
      :name => "MyString",
      :user_id => 1,
      :attachment => "MyString",
      :room_id => 1
    ))
  end

  it "renders new document form" do
    render

    assert_select "form[action=?][method=?]", documents_path, "post" do

      assert_select "input#document_name[name=?]", "document[name]"

      assert_select "input#document_user_id[name=?]", "document[user_id]"

      assert_select "input#document_attachment[name=?]", "document[attachment]"

      assert_select "input#document_room_id[name=?]", "document[room_id]"
    end
  end
end
