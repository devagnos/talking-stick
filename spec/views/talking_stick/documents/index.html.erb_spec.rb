require 'rails_helper'

RSpec.describe "documents/index", type: :view do
  before(:each) do
    assign(:documents, [
      Document.create!(
        :name => "Name",
        :user_id => 2,
        :attachment => "Attachment",
        :room_id => 3
      ),
      Document.create!(
        :name => "Name",
        :user_id => 2,
        :attachment => "Attachment",
        :room_id => 3
      )
    ])
  end

  it "renders a list of documents" do
    render
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => "Attachment".to_s, :count => 2
    assert_select "tr>td", :text => 3.to_s, :count => 2
  end
end
