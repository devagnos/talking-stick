require 'rails_helper'

RSpec.describe "documents/edit", type: :view do
  before(:each) do
    @document = assign(:document, Document.create!(
      :name => "MyString",
      :user_id => 1,
      :attachment => "MyString",
      :room_id => 1
    ))
  end

  it "renders the edit document form" do
    render

    assert_select "form[action=?][method=?]", document_path(@document), "post" do

      assert_select "input#document_name[name=?]", "document[name]"

      assert_select "input#document_user_id[name=?]", "document[user_id]"

      assert_select "input#document_attachment[name=?]", "document[attachment]"

      assert_select "input#document_room_id[name=?]", "document[room_id]"
    end
  end
end
